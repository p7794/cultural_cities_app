# Traveler's photo gallery

 Web aplikacija izrađena u svrhu projekta kolegija Programsko inženjerstvo Veleučilišta u Rijeci

 ## Korištene tehnologije

 React.js  
 Node.js  
 Express.js  
 Mongo DB 

  ## Upute za instalaciju razvojne okoline

 Instalirati najnoviju inačicu Node.js -a sa službenih stranica https://nodejs.org/en/  
  
 Postaviti se u mapu Verzija2  
 U terminalu pokrenuti komandu npm install  
 Pričekati da se postupak instalacije dovrši  
 Postaviti se u mapu Verzija2/frontend  
 U terminalu pokrenuti naredbu npm install --legacy-peer-deps  

 ## Pokretanje frontend i bekend servera

 U terminalu se postaviti u mabu Verzija2   
 Pokrenuti komandu npm run server  
 U terminalu se postaviti u mapu Verzija2/frontend  
 Pokrenuti komandu npm start 


