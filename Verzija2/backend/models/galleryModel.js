const mongoose = require('mongoose');

const GallerySchema = new mongoose.Schema({
    user: {
        required: true,
        type: String
    },
    name: {
        required: true,
        unique: true,
        type: String
    },
    description: {
        required: false,
        type: String
    },
    date: {
        required: false,
        type: Date
    },
    images: [
        {
            name: {
                required: false,
                type: String
            },
            url: {
                required: false,
                type: String
            }
        }
    ]
})

module.exports = mongoose.model('galeries', GallerySchema)