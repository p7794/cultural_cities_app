const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    country: {
        required: false,
        type: String
    },
    ageonameid: {
        required: false,
        type: Number
    },
    name: {
        required: false,
        type: String
    },
    subcountry: {
        required: false,
        type: String
    }
})

module.exports = mongoose.model('world-cities', dataSchema)