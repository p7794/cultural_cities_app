const express = require('express')
const router = express.Router()
const Model = require('../models/galleryModel');

// dohvat svih galerija GET
// /api/galeries
router.get('/getGaleries', (req, res) => {
    Model.find({}, (err, result) => {
        if (err) {
            res.json(err);
        } else {
            res.json(result);
        }
    })
})

// dohvat svih galerija prijavljenog korisnika
router.get('/getUserGalleries/:userName', (req, res) => {
    const korisnik = req.params.userName;
    Model.find({user: korisnik}, (err, result) => {
        if (err) {
            res.json(err);
        } else {
            res.json(result);
        }
    })
})

// dohvat jedne galerije
router.get('/getOneGallery/:galleryName', (req, res) => {
    const name = req.params.galleryName;
    Model.findOne({name}, (err, result) => {
        if (err) {
            res.json(err);
        } else {
            res.json(result);
        }
    })
})
// stvaranje nove galerije POST
// /api/galeries
router.post('/createGallery', async(req, res) => {
    try {
        // tijelo podataka koji se salju
    const gallery = req.body;
    const newGallery = new Model(gallery);

    // moongoose naredba za spremanje podataka u bazu
    await newGallery.save();
    res.json(gallery);
    }
    catch (error) {
        res.status(400).json('Error createGallery!!')
    }
})

//Update by ID Method
router.patch('/editGallery/:galleryName', (req, res) => {
    res.send('Update by ID API')
})

// brisanje galerije prema selektiranom nazivu
router.delete('/deleteGallery/:galleryName', async (req, res) => {
    try {
        const name = req.params.galleryName;
        const data = await Model.findOneAndDelete({name})
        res.send(`Document with ${data.name} has been deleted..`)
    }
    catch (error) {
        res.status(400).json('Error deleting gallery!!')
    }
})

module.exports = router;

