const express = require('express')
const router = express.Router()
const Model = require('../models/worldCitiesModel');

//Post Method
router.post('/post', async(req, res) => {
    const data = new Model({
        name: req.body.name
    })

    try{
        const dataToSave = await data.save();
        res.status(200).json(dataToSave)
    }
    catch(error){
        res.status(400).json({message: error.message})
    }
})

//Get all Method
router.get('/getAll', async (req, res) => {
    try{
        const data = await Model.find();
        res.json(data)
    }
    catch {
        res.status(500).json({message: error.message})
    }
    
})

//Get by ID Method
//Poziva se drzava iz baze ovisno o gradu koji se unese
router.get('/getOne/:city', async (req, res) => {
    try{
        // varijabla param(city) i varijabla u koju spremamo para NE SMIJE BITI ISTOG NAZIVA!!!
        let grad = req.params.city;
        const data = await Model.findOne({name: grad});
        res.json(data)
    }
    catch {
        res.status(500).json({message: error.message})
    }
})

//Update by ID Method
router.patch('/update/:id', (req, res) => {
    res.send('Update by ID API')
})

//Delete by ID Method
router.delete('/delete/:id', (req, res) => {
    res.send('Delete by ID API')
})

module.exports = router


