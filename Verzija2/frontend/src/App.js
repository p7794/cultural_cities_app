import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Header from './components/Header'
import Gallery from './components/Gallery'
// import Dashboard from './pages/Dashboard'
import Homepage from './components/Homepage'
import Login from './pages/Login'
import Register from './pages/Register'

function App() {
  return (
    <>
      <Router>
        <div className='container'>
          <Header />
          <Routes>
            {/* <Route path='/' element={<Dashboard />} /> */}
            <Route path='/' element={<Homepage />} />
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
            <Route path='/gallery' element={<Gallery />} />
          </Routes>
        </div>
      </Router>
      <ToastContainer />
      
    </>
  )
}

export default App
