import "./HomepageCSS.css";
import '../css/flag-icons.css';

const CountryInfo = () => {
    return (
        <div className="widget" id="country">
            <div className="countryinfo">
                <div className="flag fib fi-hr" />
                Croatia
                <div className="description">
                    <div className="left">
                        Phone code: +385<br />
                        Region: Central Europe<br />
                        Timezone: UTC+02:00<br />
                        Area: xxxxxxx
                    </div>
                    <div className="right">
                        Capital: Zagreb<br />
                        Population: 3800000<br />
                        ISO Code: HR<br />
                        Native name: Hrvatska
                    </div>
                </div>
            </div>
            <div className="currencies">
                Currencies
                <div className="description">
                    <div className="left">Name: Croatian kuna</div>
                    <div className="right">Symbol: HRK</div>
                </div>
            </div>
            <div className="bordercountry">
                Border Country
                <div id="description">ITA  SLO  BIH  SRB</div>
            </div>
        </div>
    );
}

export default CountryInfo;