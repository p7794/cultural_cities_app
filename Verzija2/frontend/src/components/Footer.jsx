import React from 'react';
import {
    MDBFooter,
    MDBContainer,
    MDBIcon,
} from 'mdb-react-ui-kit';

const Footer = () => {
    return (

        <MDBFooter className='text-center text-white' style={{ backgroundColor: '#FFFFFF' }}>
            <MDBContainer className='pt-4'>
                <section className='mb-4'>
                    <a
                        className='btn btn-link btn-floating btn-lg text-dark m-1'
                        href='#!'
                        role='button'
                        data-mdb-ripple-color='dark'
                    >
                        <MDBIcon fab className='fab fa-facebook-f' />
                    </a>

                    <a
                        className='btn btn-link btn-floating btn-lg text-dark m-1'
                        href='#!'
                        role='button'
                        data-mdb-ripple-color='dark'
                    >
                        <MDBIcon fab className='fa-twitter' />
                    </a>

                    <a
                        className='btn btn-link btn-floating btn-lg text-dark m-1'
                        href='#!'
                        role='button'
                        data-mdb-ripple-color='dark'
                    >
                        <MDBIcon fab className='fa-google' />
                    </a>

                    <a
                        className='btn btn-link btn-floating btn-lg text-dark m-1'
                        href='#!'
                        role='button'
                        data-mdb-ripple-color='dark'
                    >
                        <MDBIcon fab className='fa-instagram' />
                    </a>

                    <a
                        className='btn btn-link btn-floating btn-lg text-dark m-1'
                        href='#!'
                        role='button'
                        data-mdb-ripple-color='dark'
                    >
                        <MDBIcon fab className='fa-linkedin' />
                    </a>

                    <a
                        className='btn btn-link btn-floating btn-lg text-dark m-1'
                        href='https://gitlab.com/p7794/cultural_cities_app'
                        target="blank"
                        role='button'
                        data-mdb-ripple-color='dark'
                    >
                        <MDBIcon fab className='fa-gitlab' />
                    </a>
                </section>
            </MDBContainer>

            <div className='text-center text-dark p-3' style={{ backgroundColor: '#FAFAFA' }}>
                © 2022 Copyright:
                <a className='text-dark' href='https://www.veleri.hr/hr/'>
                    Filip Stropin
                </a>
            </div>
        </MDBFooter>

    );
}

export default Footer