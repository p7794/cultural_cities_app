import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import axios from "axios";
import './GalleryCSS.css'
import '../index.css'
import GalleryAddNewForm from './GalleryAddNewForm'
import GalleryAddNewModal from './GalleryAddNewModal'
import Footer from './Footer';
import Moment from 'moment';
import { MDBFile, MDBBtn, MDBCard, MDBCardBody, MDBCardTitle, MDBCardText, MDBCardImage } from 'mdb-react-ui-kit';

const Gallery = (props) => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const { user } = useSelector((state) => state.auth);

  // state u kojojmu se spremaju galerije iz baze
  const [listOfGalleries, setListofGalleries] = useState([]);

  // stateu kojojmu se sprema ime selektirane galerije
  const [selectedGalleryName, setSelectedGalleryName] = useState([]);

  // state u kojemu se sprema cijela selektirana galerija
  const [selectedGallery, setSelectedGallery] = useState([]);

  // loading state
  const [isLoading, setLoading] = useState(true);

  // ove se metode pozivaju svaki put kad se stranica osvjezi
  useEffect(() => {

    // provjera ako je user logiran
    if (!user) {
      navigate('/login')
    }

    // pozivanje galerija
    getUserGalleries(user.name);
  }, [user, navigate, dispatch, selectedGalleryName])


  // dohvat selektirane galerije sa liste galerija
  const handleGallerySelection = (event) => {
    setSelectedGalleryName(event.target.value);
    getOneGallery(event.target.value);
  }

  // brisanje selektirane galerije
  function deleteGallery(galleryName) {
    fetch(`http://localhost:5000/api/galeries/deleteGallery/${galleryName}`, {
      method: 'DELETE'
    }).then((result) => {
      result.json().then((resp) => {
        console.warn(resp)
      })
    })
    alert(`Deleted gallery: ${galleryName}`);
    getUserGalleries(user.name);
  }

  // dohavat svih postojecih galerija iz baze 
  const getGalleries = () => {
    axios.get("http://localhost:5000/api/galeries/getGaleries")
      .then((response) => {
        setListofGalleries(response.data)
      })
      .catch(error => console.error(`Error: ${error}`));
  }

  // dohavat svih postojecih galerija iz baze 
  const getUserGalleries = (userName) => {
    axios.get(`http://localhost:5000/api/galeries/getUserGalleries/${userName}`)
      .then((response) => {
        setListofGalleries(response.data)
      })
      .catch(error => console.error(`Error: ${error}`));
  }

  // dohvat jedne galerije iz baze
  const getOneGallery = (galleryName) => {
    axios.get(`http://localhost:5000/api/galeries/getOneGallery/${galleryName}`)
      .then((response) => {
        setSelectedGallery(response.data)
        setLoading(false);
      })
      .catch(error => console.error(`Error: ${error}`));
  }

  return (
    <>
      <GalleryAddNewModal pokazi={show} onHide={handleClose} />
      <div className='row'>
        <div className='col-sm-3'>
          <form>
            <div className='form-group'>
              <input
                type='text'
                name='text'
                id='text'
                placeholder='Search...'
              />
            </div>
            <div className='form-group'>
              <select name="galleries" id="galleries" size="15" onChange={handleGallerySelection}>
                {listOfGalleries.map((gallery) => {
                  return (
                    <option value={gallery.name}>{Moment(gallery.date).format('DD-MM-YYYY')} {gallery.name}</option>
                  )
                })};
              </select>

              <div className='form-group-add-edit-delete'>
                <button className='btn btn-block' type='button' onClick={handleShow}>
                  Add New
                </button>
                <button className='btn btn-block' type='button' >
                  Edit
                </button>
                <button className='btn btn-block' type='button' onClick={() => deleteGallery(selectedGalleryName)}>
                  Delete
                </button>
              </div>
            </div>
          </form>
        </div>

        <div name="galleries" id="galleries" className='col-sm-9'>
          {selectedGalleryName}
          {isLoading ? <div>Please select a gallery from the list or add a new one!</div> : (

            <div className='row'>
              <div className='col-sm-6'>
                <MDBCard style={{ width: "100%", marginBottom: "20px" }}>
                  <MDBCardImage src={`${selectedGallery.images[0].url}`} position='' alt='...' />
                  <MDBCardBody>
                    <MDBCardTitle></MDBCardTitle>
                    <MDBCardText>

                    </MDBCardText>
                    <MDBBtn className='text-dark' size="sm" color="light">Delete</MDBBtn>
                  </MDBCardBody>
                </MDBCard>
              </div>

              <div className='col-sm-6'>
                <MDBCard style={{ width: "100%" }}>
                  <MDBCardImage src={`${selectedGallery.images[1].url}`} position='' alt='...' />
                  <MDBCardBody>
                    <MDBCardTitle></MDBCardTitle>
                    <MDBCardText>

                    </MDBCardText>
                    <MDBBtn className='text-dark' size="sm" color="light">Delete</MDBBtn>
                  </MDBCardBody>
                </MDBCard>
              </div>

              <div className='col-sm-6'>
                <MDBCard style={{ width: "100%" }}>
                  <MDBCardImage src={`${selectedGallery.images[2].url}`} position='' alt='...' />
                  <MDBCardBody>
                    <MDBCardTitle></MDBCardTitle>
                    <MDBCardText>

                    </MDBCardText>
                    <MDBBtn className='text-dark' size="sm" color="light">Delete</MDBBtn>
                  </MDBCardBody>
                </MDBCard>
              </div>

              <div className='col-sm-6'>
                <MDBCard style={{ width: "100%" }}>
                  <MDBCardImage src={`${selectedGallery.images[3].url}`} position='' alt='...' />
                  <MDBCardBody>
                    <MDBCardTitle></MDBCardTitle>
                    <MDBCardText>

                    </MDBCardText>
                    <MDBBtn className='text-dark' size="sm" color="light">Delete</MDBBtn>
                  </MDBCardBody>
                </MDBCard>
              </div>
              {/* <img
                  src={`${selectedGallery.images[0].url}`}
                  className='img-thumbnail'
                  alt='...'
                  style={{ maxWidth: '12rem' }}

                />
              
                <img
                  src={`${selectedGallery.images[1].url}`}
                  className='img-thumbnail'
                  alt='...'
                  style={{ maxWidth: '12rem' }}
                />
              
                <img
                  src={`${selectedGallery.images[2].url}`}
                  className='img-thumbnail'
                  alt='...'
                  style={{ maxWidth: '12rem' }}
                />
              
                <img
                  src={`${selectedGallery.images[3].url}`}
                  className='img-thumbnail'
                  alt='...'
                  style={{ maxWidth: '12rem' }}
                /> */}

            </div>
          )}
          <div className='row'>
            <div className='col-sm-5'>
              <div className='file-container' style={{ paddingTop: "20px" }}>
                <MDBFile id='formFile' size='sm' />
              </div>
              </div>
              <div className='col-sm-2'>
                <div style={{ paddingTop: "20px" }}>
                  <MDBBtn className='text-dark' size="sm" color='light'>
                    Upload Image
                  </MDBBtn>
                </div>
              </div>
            </div>
          </div>
        
      </div>
      <Footer />
    </>
  )
}

export default Gallery;
