import { Button, Form } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios';
import './HomepageCSS.css';

const GalleryAddNewForm = (props, { onSubmit }) => {
    const [galleryName, setGalleryName] = useState('');
    const [galleryDescription, setGalleryDescription] = useState('');
    const [galleryDate, setGalleryDate] = useState('')
    const { user } = useSelector((state) => state.auth)

    const createGallery = () => {
        axios.post("http://localhost:5000/api/galeries/createGallery", {
            user: user.name,
            name: galleryName,
            description: galleryDescription,
            date: galleryDate,
            images: [
                {
                    name: 'Prva slika',
                    url: 'https://source.unsplash.com/random/320x240'
                },
                {
                    name: 'Druga slika',
                    url: 'https://source.unsplash.com/random/320x240'
                },
                {
                    name: 'Treca slika',
                    url: 'https://source.unsplash.com/random/320x240'
                },
                {
                    name: 'Cetvrta slika',
                    url: 'https://source.unsplash.com/random/320x240'
                }
            ]
        })
        .then((response) => {
            alert("Gallery has been ceated!");
        })
    }

    return (
        <Form onSubmit={onSubmit}>
            <Form.Group style={{padding:"10px"}} controlId="galleryName">
                <Form.Label>Gallery Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter gallery name..."
                    value={galleryName}
                    onChange={(event) => setGalleryName(event.target.value)}
                />
            </Form.Group>
            <Form.Group style={{padding:"10px"}} controlId="galleryDate">
                <Form.Label>Date</Form.Label>
                <Form.Control
                    type="date"
                    placeholder="Pick date..."
                    value={galleryDate}
                    onChange={(event) => setGalleryDate(event.target.value)}
                />
            </Form.Group>
            <Form.Group style={{padding:"10px"}} controlId="galleryDescription">
                <Form.Label>Gallery Description</Form.Label>
                <Form.Control
                    type="textarea"
                    placeholder="Short gallery description..."
                    value={galleryDescription}
                    onChange={(event) => setGalleryDescription(event.target.value)}
                />
            </Form.Group >
            <div style={{textAlign:"center", paddingTop:"30px"}} >
            <Button onClick={createGallery} style={{width:"30%"}} variant="primary" type="submit" block>
                Submit
            </Button>
            </div>
        </Form>
    );
};

export default GalleryAddNewForm;