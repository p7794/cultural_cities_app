import { Modal, Button, Form } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import GalleryAddNewForm from './GalleryAddNewForm';

const GalleryAddNewModal = (props) => {

  //handeling the modal open & close
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const onLoginFormSubmit = (e) => {
    e.preventDefault();
    handleClose();
  };

  return (
    <Modal show={props.pokazi} onHide={props.onHide}>
      <Modal.Header closeButton>
        <Modal.Title>Add New Photo Gallery</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <GalleryAddNewForm onSubmit={onLoginFormSubmit} />
      </Modal.Body>
      {/* <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close Modal
        </Button>
      </Modal.Footer> */}
    </Modal>
  )
}

export default GalleryAddNewModal;
