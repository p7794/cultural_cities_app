import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { Modal, Button, Form } from 'react-bootstrap';
//import "bootstrap/dist/css/bootstrap.css";
import './GalleryCSS.css'
import '../index.css'
import Footer from './Footer';

const LoginForm = ({ onSubmit }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  return (
    <Form onSubmit={onSubmit}>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Group>
      <Form.Group controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Remember Me!" />
      </Form.Group>
      <Button variant="primary" type="submit" block>
        Login
      </Button>
    </Form>
  );
};


const Gallery = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  //handeling the modal open & close
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const { user } = useSelector((state) => state.auth)

  const onLoginFormSubmit = (e) => {
    e.preventDefault();
    handleClose();
  };

  useEffect(() => {

    if (!user) {
      navigate('/login')
    }

    return () => {
    }
  }, [user, navigate, dispatch])

  return (
    <>
      <div className='gallery_main'>
        <div className='gallery_left'>
          <form>
            <div className='form-group'>
              <input
                type='text'
                name='text'
                id='text'
              />
            </div>
            <div className='form-group'>
              <select name="galleries" id="galleries" size="15">
                <option value="0">13.03.2018. - Rijeka</option>
              </select>
              <div className='form-group-add-edit-delete'>
                <button className='btn btn-block' type='button' onClick={handleShow}>
                  Add New
                </button>
                <button className='btn btn-block' type='button' onClick={handleShow}>
                  Edit
                </button>
                <button className='btn btn-block' type='button'>
                  Delete
                </button>
              </div>
            </div>
          </form></div>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Login Form</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <LoginForm onSubmit={onLoginFormSubmit} />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close Modal
            </Button>
          </Modal.Footer>
        </Modal>

        <div className='gallery_images'>
          13.03.2018. - Rijeka
          <div className="imgs_container">
            <img className="img_item" src="https://source.unsplash.com/random/320x240" alt="image1" />
            <img className="img_item" src="https://source.unsplash.com/random/320x240" alt="image2" />
            <img className="img_item" src="https://source.unsplash.com/random/320x240" alt="image3" />
            <img className="img_item" src="https://source.unsplash.com/random/320x240" alt="image4" />
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Gallery;
