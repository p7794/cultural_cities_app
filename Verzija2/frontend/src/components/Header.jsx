import { FaSignInAlt, FaSignOutAlt, FaUser, FaCamera } from 'react-icons/fa'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { logout, reset } from '../features/auth/authSlice'

function Header() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { user } = useSelector((state) => state.auth)

  const onLogout = () => {
    dispatch(logout())
    dispatch(reset())
    navigate('/')
  }

  return (
    <header className='header'>
      <div className='logo'>
        <Link style={{ color: '#000000' }} to='/'>Traveler's Photo Gallery</Link>

      </div>

      <ul>
        {user ? (
          <div>
            <div style={{ fontWeight: "bold" }}>
              User: {user.name}
            </div>
            <div className='btn-group'>
              <button className='btn' onClick={() => { navigate('/gallery') }}>
                <FaCamera /> Gallery
              </button>
              <button className='btn' onClick={onLogout}>
                <FaSignOutAlt /> Logout
              </button>
            </div>
          </div>
        ) : (
          <>
            <li>
              <Link to='/login'>
                <FaSignInAlt style={{ color: "black" }} /> <span style={{ color: "black" }}>Login</span>
              </Link>
            </li>
            <li>
              <Link to='/register'>
                <FaUser style={{ color: "black" }} /> <span style={{ color: "black" }}>Register</span>
              </Link>
            </li>
          </>
        )}
      </ul>
    </header>
  )
}

export default Header
