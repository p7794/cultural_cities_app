import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import Weather2 from './Weather2'

function Homepage() {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const { user } = useSelector((state) => state.auth)
  
  useEffect(() => {

    if (!user) {
      navigate('/login')
    }

    return () => {
    }
  }, [user, navigate,dispatch])

  return (
    <>
      <Weather2 />  
    </>
  )
}

export default Homepage
