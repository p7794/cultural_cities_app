import React from "react";
import './HomepageCSS.css';

import { useState } from "react";

const  InputCityForm = (props) => {

    // u ovu varijablu spremamo uneseni grad iz forme za unos grada
    const [enteredCity, setEnteredCity] = useState('');

    // funkcija koja se trigera prilikom inputa svakog znaka u input formi
    // pogledati log na console u chrome developers tool
    const cityChangeHandler = (event) => {
        // console.log(event.target.value)
        setEnteredCity(event.target.value);
    };

    //funkcija submitiranja forme
    const submitHandler = (event) => {
        // JS metoda koja brani submit formi da osvjezi stranicu
        event.preventDefault();

        // grad spremamo u ovu varijablu
        const savedCity = enteredCity;
        props.onSaveCityInput(savedCity);

        // brisanje unesenog grada nakon submita forme
        //setEnteredCity('');
    };
    
    return (
        <div className="form" style={{width: "30%"}}>
            <form onSubmit={submitHandler}>
                <div className='form-group'>
                    <input 
                        type='text'
                        value={enteredCity}
                        onChange={cityChangeHandler}
                        className='form-control'
                        id='city'
                        name='city'
                        placeholder='Enter city name...'
                    />
                </div>
                <div id="match-list"></div>
                <div className='form-group'>
                    <button type='submit' 
                            className='btn btn-block' >
                            
                        Submit
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputCityForm;