import "./HomepageCSS.css";
import React, { useEffect, useState, useRef } from "react";
import { MapContainer, TileLayer, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import axios from "axios";

delete L.Icon.Default.prototype._getIconUrl;

// workaround radi bug-a u leaflet frameworku sa markerima
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('../images/marker-icon-2x.png'),
    iconUrl: require('../images/marker-icon.png'),
    shadowUrl: require('../images/marker-shadow.png')
});


const zoom = 15;

const OpenTripMAp = (props) => {

    const [lat, setLat] = useState(props.lat);
    const [lon, setLon] = useState(props.lon);
    const [openTripAPIData, setOpenTripAPIData] = useState([]);
    const mapRef = useRef();

    useEffect(() => {
        setLat(props.lat);
        setLon(props.lon);
    }, [props.lat, props.lon])

    const getDataFromOpenTripAPI = async () => {
        await axios.get(`https://api.opentripmap.com/0.1/en/places/radius?radius=4000&lon=${lon}&lat=${lat}&limit=100&apikey=5ae2e3f221c38a28845f05b685d3f71e29d012267287c276b28735e3`)
            .then((response) => {
                const tripDataFromAPI = response.data;
                setOpenTripAPIData(tripDataFromAPI);

                console.log(openTripAPIData)
            })
            .catch(error => console.error(`Error: ${error}`));
    }


    const drawMarkersOnMAp = () => {
        const { current = {} } = mapRef;
        //const { leafletElement: map } = current;

        if (!current) return;
        //current.ClearLayers();
        const parksGeoJson = new L.GeoJSON(openTripAPIData, {
            onEachFeature: (feature = {}, layer) => {
                const { properties = {} } = feature;
                const { name } = properties;
                const { wikidata } = properties;

                if (!name) return;

                /* layer.bindPopup(`<p>${name}</p>`); */
                layer.bindPopup(`<b>${name}</b></br><a target="blank" href='http://www.wikidata.org/entity/${wikidata}' >Click for more Info!</a>`);

            }
        });
        parksGeoJson.addTo(current);
        current.setView([props.lat, props.lon]);
    }

    return (
        <section className="widget">
            <button onClick={() => { getDataFromOpenTripAPI(); drawMarkersOnMAp(); }} type='submit' className='btn btn-block'>
                Show Popular Locations
            </button>
            <MapContainer ref={mapRef} center={[props.lat, props.lon]} zoom={zoom}>
                <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution="&copy; <a href=&quot;https://www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors" />
            </MapContainer>
        </section>
    );
}
export default OpenTripMAp;