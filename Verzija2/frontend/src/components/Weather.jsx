import "./HomepageCSS.css";
import React, { useEffect, useState } from "react";
import InputCityForm from "./InputCityForm";
import Spinner from "./Spinner";

const globalWeatherData = [
    {
        "cityName": 'globalGrad',
        "temp": '666'
    }
]

export function Weather() {

    // iz nekog razloga stranica se nije renderirala ukoliko podaci ne bi bili dohvaceni iz api-ja
    // stoga je koristena varijabla isLoading koja je postavljena u true
    // pomocu nje se renderiraju oba slucaja stranice 1. samo stranice bez podataka 2. ili 2. stranica sa podacima
    const [isLoading, setLoading] = useState(true); // Loading state
    const [lat, setLat] = useState([]);
    const [long, setLong] = useState([]);
    const [weatherData, setWeatherData] = useState([]);
    let enteredCityFromInputForm ='';
    
    // podaci za trenutacnu vremensku prognozu preko geolokacije
    // ne radi svaki put, potraziti razlog
    /* useEffect(() => {
        const fetchDataOnFirstLogin = async () => {
            navigator.geolocation.getCurrentPosition(function (position) {
                setLat(position.coords.latitude);
                setLong(position.coords.longitude);
            });

            await fetch(`${process.env.REACT_APP_API_URL}/weather/?lat=${lat}&lon=${long}&units=metric&APPID=${process.env.REACT_APP_API_KEY}`)
                //await fetch(`${process.env.REACT_APP_API_URL}/weather?q=${"Rijeka"}&units=metric&APPID=${process.env.REACT_APP_API_KEY}`)
                .then(res => res.json())
                .then(result => {
                    setWeatherData(result)
                });
        }

        fetchDataOnFirstLogin();
    }, [lat, long]) */

    // hvatanje podataka sa api.openweathermap.org
    async function getWeatherData() {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/weather?q=${enteredCityFromInputForm}&units=metric&APPID=${process.env.REACT_APP_API_KEY}`)
        const data = await response.json()

        setWeatherData(data);
        globalWeatherData.cityName = data.name;
        globalWeatherData.temp = data.main.temp;
        setLoading(false);
    }

    const getEnteredCityNamefromINputForm = (enteredCity) => {
        console.log(enteredCity);
        enteredCityFromInputForm = enteredCity;
        getWeatherData();
        console.log(enteredCityFromInputForm);
    };

    // renderiranje stranice sa podacima
    // svi podaci se dohvacaju iz weatherData objekta (vidi API)
    // bez ove zastite ode sve u vrazju mater
    
        return (
            <section>
                <InputCityForm onSaveCityInput={getEnteredCityNamefromINputForm} />
                <div className="widget" id="weather">
                    <div className="weatherMain">

                        <h2 className="city">{globalWeatherData.cityName}<br />

                            <span id="datetime">{ }</span><br />
                            <span id="datetime">01:28:54 AM</span></h2>
                        <div >
                            <i className="wi wi-day-sleet-storm">

                                <span id="temperature">{globalWeatherData.temp}</span>
                                <br></br>
                                <span >Oblacno</span>
                            </i>
                        </div>
                        <div className="weatherdetails">
                            Humidity: { }%<br />
                            Wind: { }%<br />
                            Low: { }%<br />
                            High: { }%
                        </div>
                    </div>

                    <div id="future" className="weatherByDay">
                        <div className="daycontainer">
                            <h3 className="day">Mon<br /><span id="curtmp">20</span></h3>
                            <div className="weatherIcon">
                                <i className="wi wi-day-sunny"></i>
                            </div>
                            <p className="conditions">Sunny</p>
                            <p className="tempRange"><span className="high">28</span><br /><span className="low">11</span></p>
                        </div>
                        <div className="daycontainer">
                            <h3 className="day">Tue<br /><span id="curtmp">15</span></h3>
                            <div className="weatherIcon">
                                <i className="wi wi-day-sleet-storm"></i>
                            </div>
                            <p className="conditions">Storms and showers</p>
                            <p className="tempRange"><span className="high">28</span><br /><span className="low">11</span></p>
                        </div>
                        <div className="daycontainer">
                            <h3 className="day">Wed<br /><span id="curtmp">18</span></h3>
                            <div className="weatherIcon">
                                <i className="wi wi-day-cloudy"></i>
                            </div>
                            <p className="conditions">Partly Cloudy</p>
                            <p className="tempRange"><span className="high">28</span><br /><span className="low">11</span></p>
                        </div>
                        <div className="daycontainer">
                            <h3 className="day">Thr<br /><span id="curtmp">12</span></h3>
                            <div className="weatherIcon">
                                <i className="wi wi-day-lightning"></i>
                            </div>
                            <p className="conditions">Thunder storms</p>
                            <p className="tempRange"><span className="high">28</span><br /><span className="low">11</span></p>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    // renderiranje stranica bez podataka (kada smo sigurni da su podaci dohvaceni)
    
        
    
