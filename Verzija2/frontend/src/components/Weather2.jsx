import "./HomepageCSS.css";
import React, { useState, useEffect } from "react";
import InputCityForm from "./InputCityForm";
import OpenTripMAp from "./OpenTripMap";
import Spinner from "./Spinner";
import axios from "axios";
import dummyFlag from "../flags/4x3/ac.svg"
import Footer from "./Footer";

// globalne varijable
let enteredCityFromInputForm = '';
let fetchedCountryFromDB = '';
let coords = [45.3431, 14.4092];
//let weatherIcon = '10d';
//let weatherMain = 'Rain';
const Weather2 = () => {

    const [isLoading, setLoading] = useState(false); // Loading state
    const [isSubmitButtonClicked, setisSubmitButtonClicked] = useState(false);
    /* const [weatherData, setWeatherData] = useState([
        {
            "coord": {
            "lon": 14.4092,
            "lat": 45.3431
            },
            "weather": [
            {
            "id": 801,
            "main": "Clouds",
            "description": "few clouds",
            "icon": "02d"
            }
            ],
            "base": "stations",
            "main": {
            "temp": 22.84,
            "feels_like": 22.17,
            "temp_min": 12.94,
            "temp_max": 22.84,
            "pressure": 1011,
            "humidity": 38
            },
            "visibility": 10000,
            "wind": {
            "speed": 6.17,
            "deg": 50
            },
            "clouds": {
            "all": 20
            },
            "dt": 1653725239,
            "sys": {
            "type": 1,
            "id": 6385,
            "country": "HR",
            "sunrise": 1653708032,
            "sunset": 1653763143
            },
            "timezone": 7200,
            "id": 3191648,
            "name": "Rijeka",
            "cod": 200
            }
    ]); */
    const [weatherData, setWeatherData] = useState([undefined]);
    const [countryFromDB, setCountryFromDB] = useState([]);
    const [countryData, setCountryData] = useState([]);
    const [weatherIcon, setWeatherIcon] = useState('10d');
    const [weatherMain, setWeatherMain] = useState('Rain');
    

/*     useEffect(() => {
        console.log(weatherIcon, weatherMain)
        // 👇️ Check if NOT undefined or null
        getWeatherDataFromAPI();
        if (weatherData !== undefined) {
            setWeatherMain(weatherData.weather[0].description);
            setWeatherIcon(weatherData.weather[0].icon);
        }
      }, [weatherData, weatherMain, weatherIcon]);
 */
    // pozivanje openweatherAPI
    const getWeatherDataFromAPI = async () => {
        await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${enteredCityFromInputForm}&units=metric&APPID=${process.env.REACT_APP_API_KEY}`)
            .then((response) => {
                const weatherDataFromAPI = response.data;
                setWeatherData(weatherDataFromAPI);
                setWeatherIcon(weatherData.weather[0].icon);
                setWeatherMain(weatherData.weather[0].description);
                // spremi vrijednost lat i lon u varijablu coords i posalji kao prop u komponentu OpenTripMap
                coords[0] = weatherData.coord.lat;
                coords[1] = weatherData.coord.lon;
                setLoading(false);
            })
            .catch(error => console.error(`Error: ${error}`));

    }

    // pozivanje RESTCountry api

    const getCountryDataFromAPI = (countryCode) => {
        axios.get(`https://restcountries.com/v3.1/alpha/${countryCode}`)
            .then((response) => {
                const countryDataFromAPI = response.data;
                setCountryData(countryDataFromAPI);

                console.log(countryData)
                console.log(countryData[0].capital)
            })
            .catch(error => console.error(`Error: ${error}`));
    }

    // pozvati /getOne/:city i kao parametar proslijediti grad
    // treba nam drzava
    const getCountryFromDB = (city) => {
        axios.get(`http://localhost:5000/api/worldCities/getOne/${city}`)
            .then((response) => {
                setCountryFromDB(response.data);
                fetchedCountryFromDB = countryFromDB.country;
            })
            .catch(error => console.error(`Error: ${error}`));
    }

    //property InputCity form komponente - ovo se zove na klik submit
    const getEnteredCityNamefromINputForm = (enteredCity) => {
        
        console.log(enteredCity);
        enteredCityFromInputForm = enteredCity;
        
        getWeatherDataFromAPI();
        // posalji kod drzave u funkciju
        getCountryDataFromAPI(weatherData.sys.country);
        //getCountryFromDB(enteredCityFromInputForm);
        console.log(fetchedCountryFromDB);
        setisSubmitButtonClicked(true);
    };


    // postavljanje vrijednosti za ikonu i opis vremena u globane varijable
    // postoji problem sa ispisom kada su vuku iz objekta koji je vratio api
    
    return (
        <section>
            <InputCityForm onSaveCityInput={getEnteredCityNamefromINputForm} />
            {isLoading ? (
                <Spinner />
            ) : (
                <section>
                    <div className="widget" id="weather">
                        <div className="weatherMain">
                            <h2 className="city">{weatherData?.name || "Loading"}<br />
                                <span id="datetime">{ }</span><br />
                                <span id="datetime">{Date().toLocaleString()}</span></h2>
                            <div >
                                <div className="wi wi-day-sleet-storm">
                                    <span id="temperature">{weatherData.main?.temp || "Loading"}</span>
                                    <br></br>
                                    
                                    {/* <h3 >Rain</h3><br></br> */}
                                    {/* <h4 >{weatherData?.weather[0].description || "Loading"}</h4> */}
                                    
                                    <h3 >{weatherMain}</h3><br></br>
                                    <div className="row">
                                        <img src={`http://openweathermap.org/img/wn/${weatherIcon}@2x.png`}alt="weather-icon"></img>
                                        {/* <img className="col-md-9" src={`http://openweathermap.org/img/wn/10d@4x.png`} alt="weather-icon"></img> */}
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <div className="weatherdetails">
                                Humidity:{weatherData.main?.humidity || "Loading"}%<br />

                                Pressure:{weatherData.main?.pressure || "Loading"}Pa<br />

                                Wind:{weatherData.wind?.speed || "Loading"}m/s<br />

                                Low:{weatherData.main?.temp_min || "Loading"}C<br />

                                High:{weatherData.main?.temp_max || "Loading"}C
                            </div>
                        </div>

                    </div>

                    <div className="widget" id="country">
                        <div className="countryinfo">

                            <img className="flag" src={countryData[0]?.flags.png || dummyFlag} alt={"hej"} />
                            {countryData[0]?.altSpellings[2] || "Loading"} <br />
                            <h6>{countryData[0]?.altSpellings[1] || "Loading"}</h6>
                            <div className="description">
                                <div className="left">
                                    Phone code: {countryData[0]?.idd.root || "Loading"}{countryData[0]?.idd.suffixes || "Loading"}<br />
                                    Region: {countryData[0]?.region || "Loading"}<br />
                                    Subregion: {countryData[0]?.subregion || "Loading"}<br />
                                    Timezone: {countryData[0]?.timezones || "Loading"}

                                </div>
                                <div className="right">
                                    Capital: {countryData[0]?.capital || "Loading"}<br />
                                    Population: {countryData[0]?.population || "Loading"}<br />
                                    Internet TLD: {countryData[0]?.tld || "Loading"}<br />
                                    Car Sign: {countryData[0]?.car.signs || "Loading"}
                                </div>
                            </div>
                        </div>
                        <div className="currencies">
                            State status
                            <div className="description">
                                <div className="left">Independent: {countryData[0]?.independent || "Loading"}</div>
                                <div className="right">UN Member: {countryData[0]?.unMember || "Loading"}</div>
                            </div>
                        </div>
                        <div className="bordercountry">
                            Border Country
                            <div id="description">{countryData[0]?.borders[0] || "Loading"} {countryData[0]?.borders[1] || "Loading"} {countryData[0]?.borders[2] || "Loading"} {countryData[0]?.borders[3] || "Loading"} {countryData[0]?.borders[4] || "Loading"} {countryData[0]?.borders[5] || "Loading"}</div>
                        </div>
                    </div>
                </section>
            )}
            <OpenTripMAp lat={coords[0]} lon={coords[1]} isButtonClicked={isSubmitButtonClicked} />
            <Footer />
        </section>
    );
};

export default Weather2;