import './App.css';
import GalleryPage from './pages/GalleryPage';
import UserAccount from './pages/UserAccount';
import HomePage from './pages/HomePage';
import React, { Component } from 'react';
//import { BrowserRouter as Router, Route } from "react-router-dom";

// react HOOK - pogledati tutorial za hooks
//import { useState, useEffect } from "react";
//import Axios from "axios";
import { Route, Routes} from 'react-router-dom';

// Bootstrap css style
import 'bootstrap/dist/css/bootstrap.min.css';

// import components
//import AddUser from './components/AddUser';
//import MainHeader from './components/MainHeader';
import NavBar from './components/NavBar';
import Footer from './components/Footer';
//import NavbarMaterial from './components/layout/NavbarMaterial';
import LandingMmaterial from './components/layout/LandingMaterial';
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";


function App() {
  return (
    <div>
      <main>
        <Routes>
          <Route path='/' element={<LandingMmaterial />} />
          <Route path='/register' element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route path='/home' element={<HomePage />} />
          <Route path='/gallery' element={<GalleryPage />} />
          <Route path='/useraccount' element={<UserAccount />} />
        </Routes>
      </main>
      
    </div>
  );
}

export default App;


// our-domain.com/home => otvara se Home Page
// our.domain.com/gallery => otvara se foto galerija
// our.domain.com/useraccount => otvara se rad sa korisnickim racunom