import './AddUser.css';
// react HOOK - pogledati tutorial za hooks
import { useState, useEffect } from "react";
import Axios from "axios";

function AddUser() {
    // ovdje koristimo uvezeni hook useState
    // preko ovih varijabli se prenose varijable iz baze na frontend
    const [listOfUsers, setListOfUsers] = useState([]);
    const [ime, setIme] = useState("");
    const [prezime, setPrezime] = useState("");
    const [username, setUsername] = useState("");
    const [e_mail, setE_mail] = useState("");

    // axios je modul koji omogucuje jednostavnije pozivanje rest api-ja
    // metoda ua pozivanje getUsers REST-a
    useEffect(() => {
        Axios.get("http://localhost:3001/getUsers").then((response) => {
            setListOfUsers(response.data)
        })
    }, []);

    // metoda za pozivanje createUser REST-a
    const createUser = () => {
        Axios.post("http://localhost:3001/createUser", {
            ime: ime,
            prezime: prezime,
            userName: username,
            e_mail: e_mail,
        }).then((response) => {
            setListOfUsers([
                ...listOfUsers,
                {
                    ime,
                    prezime,
                    username,
                    e_mail,
                },
            ]);
        });
    }

    return (
        <div className="App">
            <div className='userDisplay'>
                {listOfUsers.map((user) => {
                    return (
                        <div>
                            <h4>Ime: {user.ime}</h4>
                            <h4>Prezime: {user.prezime}</h4>
                            <h4>Username: {user.username}</h4>
                            <h4>E-mail: {user.e_mail}</h4>
                        </div>
                    );
                })}
            </div>

            <div>
                <input type="text" placeholder='Ime...' onChange={(event) => {
                    setIme(event.target.value);
                }}></input>

                <input type="text" placeholder='Prezime...' onChange={(event) => {
                    setPrezime(event.target.value);
                }}></input>

                <input type="text" placeholder='User name...' onChange={(event) => {
                    setUsername(event.target.value);
                }}></input>

                <input type="text" placeholder='E-mail...' onChange={(event) => {
                    setE_mail(event.target.value);
                }}></input>

                <button onClick={createUser}>Create user</button>
            </div>
        </div>
    );
}

export default AddUser;