
import { Container } from 'react-bootstrap';

const Footer = () =>
    <Container>
        <footer className="page-footer font-small blue pt-4">


            <div className="footer-copyright text-center py-3">© 2022 Copyright:
                Web Travel Photo Gallery
            </div>

        </footer>
    </Container>

export default Footer;