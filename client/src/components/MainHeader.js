// modul koji zove linkove bez refresanja ciele stranice, na ovaj nacin ostaju nam stanja u memoriji lokalnoj memoriji
// npr web shop kosarica...
import { NavLink } from 'react-router-dom';
import './MainHeader.css';

const MainHeader = () => {
    return (
        <header className="header">
            <nav>
                <ul>
                    <li>
                        <NavLink activeClassName="active" to='/home'>Home</NavLink>
                    </li>
                    <li>
                        <NavLink activeClassName="active" to='/gallery'>Photo Gallery</NavLink>
                    </li>
                    <li>
                        <NavLink activeClassName="active" to='/useraccount'>User Account</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    )
};

export default MainHeader;