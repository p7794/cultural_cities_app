import "../App.css";

export function Search() {
    return (
        <div className='search'>
            <input type="text" placeholder='Start typing city name...' className='inputCity'></input>
            <button type='submit' className='submit'>Submit</button>
        </div>
    );
}