import "../App.css";
import { WiDaySunny } from 'weather-icons-react';
export function Weather() {
    return (
        <div className="widget" id="weather">
            <div className="weatherMain">
                <h3 class="city">Zagreb<br />
                    <span id="datetime">Sunday, May 1 2022.</span><br />
                    <span id="datetime">01:28:54 AM</span></h3>
                <div class="weatherIcon">
                    <i class="wi wi-day-sleet-storm">
                        <span id="temperature">20</span>
                        <span id="conditions">Storm</span>
                    </i>
                </div>
                <div class="weatherdetails">
                    Humidity:<br />
                    Wind:<br />
                    Low:<br />
                    High:
                </div>
            </div>

            <div id="future" className="weatherByDay">
                <div className="daycontainer">
                    <h3 class="day">Mon<br /><span id="curtmp">20</span></h3>
                    <div class="weatherIcon">
                        <WiDaySunny size={10} color='white' />
                    </div>
                    <p class="conditions">Sunny</p>
                    <p class="tempRange"><span class="high">28</span><br /><span class="low">11</span></p>
                </div>
                <div className="daycontainer">
                    <h3 class="day">Tue<br /><span id="curtmp">15</span></h3>
                    <div class="weatherIcon">
                        <i class="wi wi-day-sleet-storm"></i>
                    </div>
                    <p class="conditions">Storms and showers</p>
                    <p class="tempRange"><span class="high">28</span><br /><span class="low">11</span></p>
                </div>
                <div className="daycontainer">
                    <h3 class="day">Wed<br /><span id="curtmp">18</span></h3>
                    <div class="weatherIcon">
                        <i class="wi wi-day-cloudy"></i>
                    </div>
                    <p class="conditions">Partly Cloudy</p>
                    <p class="tempRange"><span class="high">28</span><br /><span class="low">11</span></p>
                </div>
                <div className="daycontainer">
                    <h3 class="day">Thr<br /><span id="curtmp">12</span></h3>
                    <div class="weatherIcon">
                        <i class="wi wi-day-lightning"></i>
                    </div>
                    <p class="conditions">Thunder storms</p>
                    <p class="tempRange"><span class="high">28</span><br /><span class="low">11</span></p>
                </div>
            </div>
        </div>
    );
}