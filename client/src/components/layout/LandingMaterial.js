import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";
import AppLogo from '../../assets/images/AppLogo.jpg';
import './LandingMaterial.css';

class LandingMaterial extends Component {
  render() {
    return (
      <Container>
        <Row className="justify-content-md-center">
          <Col md={{ span: 6, offset: 0 }}>
            <Row className="justify-content-md-center">
              <img className="img-fluid" src={AppLogo} alt="fireSpot" style={{
                heigh: "auto",
                width: "auto"
              }} />
            </Row>
            <Row>
            <div className ="col-md-6 text-center">
                <Button as={Link} to="/register"
                  style={{
                    width: "140px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px"
                  }}>
                  Register
                </Button>
                </div>
                <div className="col-md-6 text-center" >
                <Button as={Link} to="/login"
                  style={{
                    width: "140px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px"
                  }}>
                  Login
                </Button>
                </div>
              
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default LandingMaterial;