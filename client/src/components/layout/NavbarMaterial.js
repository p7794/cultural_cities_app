import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";

const NavbarMaterial = () => {
    return (
      <nav>
        <div class="nav-wrapper">
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><NavLink activeClassName="active" to='/home'>Home</NavLink></li>
            <li><NavLink activeClassName="active" to='/gallery'>Photo Gallery</NavLink></li>
            <li><NavLink activeClassName="active" to='/useraccount'>User Account</NavLink></li>
          </ul>
        </div>
      </nav>
    );
  }
export default NavbarMaterial;

