import './GalleryPage.css';
import Papiga from '../assets/images/papiga.jpg';
import { Button, Card, Container } from 'react-bootstrap';

const GalleryPage = () => {
    return (
        <Container>
            <div>
                <h1>Ovdje cemo napraviti galeriju slika</h1>
                <Card>
                    <img src={Papiga} alt="fireSpot" />
                </Card>
                <Button>Test dugme</Button>
                <Button>Test dugme</Button>
            </div>
        </Container>

    );
};

export default GalleryPage;