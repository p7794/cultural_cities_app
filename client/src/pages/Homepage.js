import './HomePage.css';
import { Button, Card, Container } from 'react-bootstrap';
// import { MapContainer, TileLayer, useMap, Marker, Popup } from 'react-leaflet';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';


const HomePage = () => {
    return (
        <Container>
        <div>
            <NavBar />
            <div>
                <h1>Web Travel Home Page </h1>
                
            </div>
            <Footer />
        </div>
        </Container>
    );
};

export default HomePage;