// import instaliranih modula
const express = require("express");
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require("body-parser");
const passport = require("passport");

const users = require("./routes/api/users");

const app = express();

app.use(express.json());
app.use(cors());

// Bodyparser middleware
app.use(
    bodyParser.urlencoded({
      extended: false
    })
  );
  app.use(bodyParser.json());

// import svih modela iz mape models
// TODO importirati sve ostale kada budu kreirani
const UserModel = require('./models/Korisnici');

// DB Config
const db = require("./config/keys").mongoURI;

// konekcija na mongo.db atlas cluster i provjera spajanja
mongoose.connect("mongodb+srv://admin:Bljak_1234@cluster0.xzrex.mongodb.net/travel_foto_gallery?retryWrites=true&w=majority")
    .then(() => console.log("MongoDB successfully connected"))
    .catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());
// Passport config
require("./config/passport")(passport);
// Routes
app.use("/api/users", users);

// get metoda uzima vrijednosti iz baze podataka
app.get("/getUsers", (req, res) => {
    UserModel.find({}, (err, result) => {
        if (err) {
            res.json(err);
        } else {
            res.json(result);
        }
    });
});

// post metoda dodaje podatke u bazu podataka
app.post("/createUser", async (req, res) => {
    const user = req.body;
    const newUser = new UserModel(user);
    await newUser.save();

    res.json(user);
});

// startanje servera
app.listen(3001, () => {
    console.log("SERVER RUNS OK!");
});
